import 'package:flutter/material.dart';

class MoviePoster extends StatelessWidget {
  final String moviePosterURL;
  MoviePoster({@required this.moviePosterURL});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      width: 130.0,
      height: 200.0,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(moviePosterURL),
            fit: BoxFit.cover,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0)),
    );
  }
}
