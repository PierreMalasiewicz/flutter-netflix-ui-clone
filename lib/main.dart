import 'package:first_app/HomeScreen.dart';
import 'package:first_app/TmdbFormField.dart';
import 'package:flutter/material.dart';
import 'package:first_app/Film/MovieGetter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.black,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController textControllerMail = TextEditingController();
  FocusNode focusNodeMail = FocusNode();
  TextEditingController textControllerPass = TextEditingController();
  FocusNode focusNodePass = FocusNode();

  @override
  void dispose() {
    textControllerMail.dispose();
    focusNodeMail.dispose();
    textControllerPass.dispose();
    focusNodePass.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MovieGetter.playToudoum();
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              title: Text("Naiteflisque"),
              backgroundColor: Colors.red,
            ),
            body: SingleChildScrollView(
                child: Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      Image.network(
                        "https://www.journaldugeek.com/content/uploads/2019/12/netflix-logo-2.jpg",
                        fit: BoxFit.cover,
                      ),
                      Padding(
                        padding: EdgeInsets.all(25),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: TextFormField(
                          //Flutter bug avec un tmdbformfield, obligé de remettre le contenu du widget ici pour que ça marche
                          controller: textControllerMail,
                          focusNode: focusNodeMail,
                          decoration: InputDecoration(
                            focusColor: Colors.white,
                            hoverColor: Colors.white,
                            hintText: "E-Mail",
                            contentPadding: const EdgeInsets.all(20.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            disabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            focusedErrorBorder: InputBorder.none,
                            filled: true,
                            fillColor: Colors.white24,
                          ),
                          validator: (value) {
                            bool emailValid = RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+") //Regex pour détecter les adresses mail
                                .hasMatch(value);
                            if (!emailValid) {
                              return 'Please enter a valid email adress';
                            }
                            return null;
                          },
                          onSaved: (String value) => value.trim(),
                          keyboardType: TextInputType.emailAddress,
                          cursorColor: Colors.white,
                          onEditingComplete: focusNodeMail.nextFocus,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.all(20),
                          child: TmdbFormField(
                            controller: textControllerPass,
                            focusNode: focusNodePass,
                            isPassword: true,
                            hintText: "Password",
                            validator: (value) {
                              if (value.length < 6) {
                                return 'Please enter your password (6 characters min.)';
                              }
                              return null;
                            },
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.red),
                          ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (ctxt) => HomeScreen()));
                            }
                          },
                          child: Text('Connection'),
                        ),
                      ),
                    ])))));
  }
}
