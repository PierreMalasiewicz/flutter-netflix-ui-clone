import 'package:first_app/Objects/Actor.dart';
import 'package:first_app/Objects/Movie.dart';
import 'package:first_app/Objects/Genre.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

class MovieGetter {
  var apiKey = "62feaff3d2cf094a340f530fbf25bde9";

  static Future<List<Movie>> fetchMovieData(String sort) async {
    var popularMoviesURL =
        "https://api.themoviedb.org/3/discover/movie?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US&sort_by=" +
            sort +
            "&include_adult=false&include_video=false&page=1";
    if (sort == "vote_average.desc") {
      popularMoviesURL = popularMoviesURL + "&vote_count.gte=5000";
    }
    final response = await http.get(popularMoviesURL);
    List<Movie> movies = [];
    if (response.statusCode == 200) {
      var jsonResult = json.decode(response.body);
      List<dynamic> results = jsonResult['results'];
      for (var result in results) {
        Movie movie = Movie.fromJson(result);
        movies.add(movie);
        print(movie.title);
      }
      return movies;
    } else {
      log("ERROR");
      log(response.statusCode.toString());
      return null;
    }
  }

  static Future<String> fetchActors(String id, String type) async {
    var actorsURL = "https://api.themoviedb.org/3/" +
        type +
        "/" +
        id +
        "/credits?api_key=62feaff3d2cf094a340f530fbf25bde9";
    final response = await http.get(actorsURL);
    List<Actor> actors = [];
    if (response.statusCode == 200) {
      var jsonResult = json.decode(response.body);
      List<dynamic> cast = jsonResult['cast'];
      for (var castMember in cast) {
        Actor actor = Actor.fromJson(castMember);
        actors.add(actor);
        print(actor.name);
      }

      String actorList = "";
      for (int i = 0; i < actors.length; i++) {
        if (i == 0) {
          actorList = actorList + actors[i].name;
        } else {
          actorList = actorList + ", " + actors[i].name;
        }
        if (i > 4) {
          break;
        }
      }
      return actorList;
    } else {
      log("ERROR");
      log(response.statusCode.toString());
      return null;
    }
  }

  static Future<List<String>> fetchGenres(
      List<dynamic> genresID, String type) async {
    var genresURL = "https://api.themoviedb.org/3/genre/" +
        type +
        "/list?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US";
    final response = await http.get(genresURL);
    List<String> listeGenres = [];
    List<Genre> genres = [];
    if (response.statusCode == 200) {
      var jsonResult = json.decode(response.body);
      List<dynamic> genresJson = jsonResult['genres'];
      for (var genrejson in genresJson) {
        Genre genre = Genre.fromJson(genrejson);
        genres.add(genre);
      }
      for (var genreIdFilm in genresID) {
        for (Genre genreDansListe in genres) {
          if (genreDansListe.id == genreIdFilm) {
            listeGenres.add(genreDansListe.name);
            print(genreDansListe.name);
          }
        }
      }

      return listeGenres;
    } else {
      log("ERROR");
      log(response.statusCode.toString());
      return null;
    }
  }

  static playToudoum() async {
    Soundpool pool = Soundpool(streamType: StreamType.music);

    int soundId =
        await rootBundle.load("assets/toudoum.m4a").then((ByteData soundData) {
      return pool.load(soundData);
    });
    int streamId = await pool.play(soundId);
  }
}
