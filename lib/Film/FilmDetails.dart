import 'package:first_app/Objects/Movie.dart';
import 'package:flutter/material.dart';
import 'package:first_app/Film/MovieGetter.dart';

class FilmDetail extends StatelessWidget {
  final Movie details;
  FilmDetail({Key key, @required this.details}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.black,
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 350,
            pinned: true,
            stretch: true,
            onStretchTrigger: () {
              // Function callback for stretch
              return;
            },
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              centerTitle: false,
              titlePadding: EdgeInsets.all(10.0),
              title: Text(details.title, style: TextStyle(color: Colors.white)),
              background: Stack(
                fit: StackFit.expand,
                children: [
                  Hero(
                      tag: details.id,
                      child: Image.network(
                        details.backdrop_path,
                        fit: BoxFit.cover,
                      )),
                  const DecoratedBox(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(0.0, 1),
                        end: Alignment(0.0, 0.5),
                        colors: <Color>[
                          Color(0xFF000000),
                          Color(0x00000000),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverFillRemaining(
            child: Container(
              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child:
                            Text("12+", style: TextStyle(color: Colors.white)),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(details.release_date.substring(0, 4),
                            style: TextStyle(color: Colors.white)),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text("⭐ " + details.vote_average.toString(),
                            style: TextStyle(color: Colors.yellow)),
                      )
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 20.0),
                      height: 45.0,
                      child: FutureBuilder(
                          future: MovieGetter.fetchGenres(
                              details.genre_ids, "movie"),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            } else {
                              return ListView.builder(
                                  physics: const BouncingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: snapshot.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Card(
                                        child: Container(
                                      child: Text(snapshot.data[index],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold)),
                                      decoration: new BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5.0))),
                                      padding: EdgeInsets.all(10),
                                    ));
                                  });
                            }
                          })),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: FutureBuilder(
                          future: MovieGetter.fetchActors(
                              details.id.toString(), "movie"),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            } else {
                              return RichText(
                                textAlign: TextAlign.left,
                                text: new TextSpan(
                                  style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                  ),
                                  children: <TextSpan>[
                                    new TextSpan(
                                        text: 'Cast : ',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    new TextSpan(
                                      text: snapshot.data,
                                    )
                                  ],
                                ),
                              );
                            }
                          })),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text("Summary",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold))),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(details.overview,
                          maxLines: 6,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white))),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
