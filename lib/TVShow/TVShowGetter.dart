import 'package:first_app/Objects/TV%20Show.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';

class TVShowGetter {
  var apiKey = "62feaff3d2cf094a340f530fbf25bde9";

  static Future<List<TVShow>> fetchTVShowData() async {
    var popularMoviesURL =
        "https://api.themoviedb.org/3/discover/tv?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US&sort_by=popularity.desc&page=1&timezone=America%2FNew_York&include_null_first_air_dates=false";
    final response = await http.get(popularMoviesURL);
    List<TVShow> tvShows = [];

    if (response.statusCode == 200) {
      var jsonResult = json.decode(response.body);
      List<dynamic> results = jsonResult['results'];

      for (var result in results) {
        TVShow tvShow = TVShow.fromJson(result);
        tvShows.add(tvShow);
        print(tvShow.name);
      }
      return tvShows;
    } else {
      log("ERROR");
      log(response.statusCode.toString());
      return null;
    }
  }
}
