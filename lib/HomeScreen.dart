import 'package:first_app/Objects/Movie.dart';
import 'package:flutter/material.dart';
import 'package:first_app/Film/MovieGetter.dart';
import 'package:first_app/Film/FilmDetails.dart';
import 'package:first_app/Objects/Movie.dart';
import 'package:first_app/Objects/TV%20Show.dart';
import 'package:first_app/TVShow/TVShowDetails.dart';
import 'package:first_app/TVShow/TVShowGetter.dart';
import 'package:flutter/material.dart';
import 'package:first_app/Film/MovieGetter.dart';
import 'package:first_app/MoviePoster.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text("Naiteflisque"),
        ),
        body: new ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                "Popular Movies",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              height: 200.0,
              child: FutureBuilder<List<Movie>>(
                  future: MovieGetter.fetchMovieData("popularity.desc"),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          transitionDuration:
                                              Duration(milliseconds: 500),
                                          pageBuilder: (_, __, ___) =>
                                              FilmDetail(
                                                details: snapshot.data[index],
                                              )));
                                },
                                child: Hero(
                                  tag: snapshot.data[index].id,
                                  child: MoviePoster(
                                      moviePosterURL:
                                          snapshot.data[index].poster_path),
                                ));
                          });
                    }
                  }),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                "Popular TV Shows",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              height: 200.0,
              child: FutureBuilder<List<TVShow>>(
                  future: TVShowGetter.fetchTVShowData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          transitionDuration:
                                              Duration(milliseconds: 500),
                                          pageBuilder: (_, __, ___) =>
                                              TVShowDetail(
                                                details: snapshot.data[index],
                                              )));
                                },
                                child: Hero(
                                  tag: snapshot.data[index].id,
                                  child: MoviePoster(
                                      moviePosterURL:
                                          snapshot.data[index].poster_path),
                                ));
                          });
                    }
                  }),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                "Best Movies",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              height: 200.0,
              child: FutureBuilder<List<Movie>>(
                  future: MovieGetter.fetchMovieData("vote_average.desc"),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          transitionDuration:
                                              Duration(milliseconds: 500),
                                          pageBuilder: (_, __, ___) =>
                                              new FilmDetail(
                                                  details:
                                                      snapshot.data[index])));
                                },
                                child: Hero(
                                  tag: snapshot.data[index].id,
                                  child: MoviePoster(
                                      moviePosterURL:
                                          snapshot.data[index].poster_path),
                                ));
                          });
                    }
                  }),
            ),
          ],
        ));
  }
}
