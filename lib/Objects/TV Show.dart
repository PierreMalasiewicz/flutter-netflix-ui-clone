class TVShow {
  var name;
  var poster_path;
  var backdrop_path;
  var vote_average;
  var overview;
  var first_air_date;
  var id;
  List<dynamic> genre_ids;

  TVShow(var name, var poster_path, var backdrop_path, var vote_average,
      var overview, var first_air_date, var id) {
    this.name = name;
    this.poster_path = poster_path;
    this.backdrop_path = backdrop_path;
    this.vote_average = vote_average;
    this.overview = overview;
    this.first_air_date = first_air_date;
    this.genre_ids = genre_ids;
    this.id = id;
  }

  TVShow.fromJson(Map json) {
    this.id = json['id'];
    this.name = json['name'];
    this.poster_path = json['poster_path'];
    this.backdrop_path = json['backdrop_path'];
    this.vote_average = json['vote_average'];
    this.overview = json['overview'];
    this.genre_ids = json['genre_ids'];
    this.first_air_date = json['first_air_date'];
    if (this.poster_path == "" || this.poster_path == null) {
      this.poster_path =
          "https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg";
    } else {
      this.poster_path = "https://image.tmdb.org/t/p/w1280/" + this.poster_path;
    }
    if (this.backdrop_path == "" || this.backdrop_path == null) {
      this.backdrop_path =
          "https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg";
    } else {
      this.backdrop_path =
          "https://image.tmdb.org/t/p/w1280/" + this.backdrop_path;
    }
    this.vote_average = json['vote_average'];
    this.overview = json['overview'];
  }
}
