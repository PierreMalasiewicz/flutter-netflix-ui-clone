class Actor {
  var character;
  var name;

  Actor(var name, var character) {
    this.name = name;
    this.character = character;
  }

  Actor.fromJson(Map json) {
    this.name = json['name'];
    this.character = json['character'];
  }
}
