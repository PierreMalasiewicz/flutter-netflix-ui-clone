class Genre {
  var id;
  var name;

  Genre(var name, var id) {
    this.name = name;
    this.id = id;
  }

  Genre.fromJson(Map json) {
    this.name = json['name'];
    this.id = json['id'];
  }
}
