class Movie {
  var popularity;
  var vote_count;
  var video;
  var poster_path;
  var id;
  var adult;
  var backdrop_path;
  var original_language;
  var original_title;
  List<dynamic> genre_ids;
  var title;
  var vote_average;
  var overview;
  var release_date;

  Movie(
      var popularity,
      var vote_count,
      var video,
      var poster_path,
      var id,
      var adult,
      var backdrop_path,
      var original_language,
      var original_title,
      List<dynamic> genre_ids,
      var title,
      var vote_average,
      var overview,
      var release_date) {
    this.popularity = popularity;
    this.vote_count = vote_count;
    this.video = video;
    this.poster_path = poster_path;
    this.id = id;
    this.adult = adult;
    this.backdrop_path = backdrop_path;
    this.original_language = original_language;
    this.original_title = original_title;
    this.genre_ids = genre_ids;
    this.title = title;
    this.vote_average = vote_average;
    this.overview = overview;
    this.release_date = release_date;
  }

  Movie.fromJson(Map json) {
    this.popularity = json['popularity'];
    this.vote_count = json['vote_count'];
    this.video = json['video'];
    this.poster_path = json['poster_path'];
    if (this.poster_path == "" || this.poster_path == null) {
      this.poster_path =
          "https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg";
    } else {
      this.poster_path = "https://image.tmdb.org/t/p/w1280/" + this.poster_path;
    }

    this.id = json['id'];
    this.adult = json['adult'];
    this.backdrop_path = json['backdrop_path'];
    if (this.backdrop_path == "" || this.backdrop_path == null) {
      this.backdrop_path =
          "https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg";
    } else {
      this.backdrop_path =
          "https://image.tmdb.org/t/p/w1280/" + this.backdrop_path;
    }
    this.original_language = json['original_language'];
    this.original_title = json['original_title'];
    this.genre_ids = json['genre_ids'];
    this.title = json['title'];
    this.vote_average = json['vote_average'];
    this.overview = json['overview'];
    this.release_date = json['release_date'];
    if (this.release_date == "" || this.release_date == null) {
      this.release_date = "    ";
    }
  }

  Map toJson() {
    return {
      'popularity': popularity,
      'vote_count': vote_count,
      'video': video,
      'poster_path': poster_path,
      'id': id,
      'adult': adult,
      'backdrop_path': backdrop_path,
      'original_language': original_language,
      'original_title': original_title,
      'genre_ids': genre_ids,
      'title': title,
      'vote_average': vote_average,
      'overview': overview,
      'release_date': release_date,
    };
  }
}
